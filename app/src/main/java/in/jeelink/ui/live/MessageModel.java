package in.jeelink.ui.live;

import java.util.List;

public class MessageModel {

    /**
     * status : true
     * message : successfully.
     * data : [{"id":"32","batch_id":"17","user_id":"0","name":"admin","is_admin":"1","text":"hi","created_at":"2021-05-01 17:10:20"},{"id":"33","batch_id":"17","user_id":"75","name":null,"is_admin":"0","text":"fg","created_at":"2021-05-01 05:18:26"},{"id":"34","batch_id":"17","user_id":"75","name":null,"is_admin":"0","text":"gh","created_at":"2021-05-01 05:18:45"},{"id":"35","batch_id":"17","user_id":"75","name":null,"is_admin":"0","text":"gy","created_at":"2021-05-01 05:19:06"},{"id":"36","batch_id":"17","user_id":"76","name":null,"is_admin":"0","text":"hi","created_at":"2021-05-01 05:19:52"},{"id":"37","batch_id":"17","user_id":"76","name":null,"is_admin":"0","text":"g","created_at":"2021-05-01 05:20:17"},{"id":"38","batch_id":"17","user_id":"76","name":null,"is_admin":"0","text":"fr","created_at":"2021-05-01 05:20:20"},{"id":"39","batch_id":"17","user_id":"76","name":null,"is_admin":"0","text":"cdh","created_at":"2021-05-01 05:23:22"},{"id":"40","batch_id":"17","user_id":"0","name":"admin","is_admin":"1","text":"hi","created_at":"2021-05-01 17:34:34"},{"id":"41","batch_id":"17","user_id":"76","name":null,"is_admin":"0","text":"hii","created_at":"2021-05-01 05:34:46"},{"id":"42","batch_id":"17","user_id":"76","name":null,"is_admin":"0","text":"dhh","created_at":"2021-05-01 05:34:56"}]
     */

    private boolean status;
    private String message;
    /**
     * id : 32
     * batch_id : 17
     * user_id : 0
     * name : admin
     * is_admin : 1
     * text : hi
     * created_at : 2021-05-01 17:10:20
     */

    private List<DataBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String id;
        private String batch_id;
        private String user_id;
        private String name;
        private String is_admin;
        private String text;
        private String created_at;
        private String msg_type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBatch_id() {
            return batch_id;
        }

        public void setBatch_id(String batch_id) {
            this.batch_id = batch_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIs_admin() {
            return is_admin;
        }

        public void setIs_admin(String is_admin) {
            this.is_admin = is_admin;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getMsg_type() {
            return msg_type;
        }

        public void setMsg_type(String msg_type) {
            this.msg_type = msg_type;
        }
    }
}