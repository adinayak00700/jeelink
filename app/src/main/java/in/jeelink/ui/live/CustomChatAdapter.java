package in.jeelink.ui.live;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.eacademy.R;


public class CustomChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private Callbackk callbackk;
    ArrayList<MessageModel.DataBean> list=new ArrayList<>();
    public static final int MESSAGE_TYPE_IN = 1;
    public static final int MESSAGE_TYPE_OUT = 2;
    public interface Callbackk{
        void  onClickOnVoiceMessage(String audiopath);
    }


    public CustomChatAdapter(Context context,Callbackk callbackk) { // you can pass other parameters in constructor
        this.context = context;
        this.callbackk = callbackk;
    }

    private class MessageInViewHolder extends RecyclerView.ViewHolder {


        TextView messageTV,dateTV;
        ImageView image;
        MessageInViewHolder(final View itemView) {
            super(itemView);
            messageTV = itemView.findViewById(R.id.text_message_body);
            dateTV = itemView.findViewById(R.id.text_message_time);
          //  image = itemView.findViewById(R.id.ivPlay);
        }
        void bind(int position) {
            MessageModel.DataBean messageModel = list.get(position);
            messageTV.setText(messageModel.getText());
            dateTV.setText(PriceFormatter.get12HrFormatTime(list.get(position).getCreated_at().substring(11,16))+" | "+PriceFormatter.formatDateMonnth(list.get(position).getCreated_at().substring(0,10)));
          /* image.setVisibility(View.GONE);*/
            messageTV.setVisibility(View.VISIBLE);
            dateTV.setVisibility(View.VISIBLE);
            if (messageModel.getMsg_type().equals("2")){
              /*  image.setVisibility(View.VISIBLE);*/
                messageTV.setVisibility(View.GONE);
                dateTV.setVisibility(View.GONE);
                //  Glide.with(context).load(messageModel.getMessage()).into(image);
          /*      image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                   callbackk.onClickOnVoiceMessage( messageModel.getText());
                    }
                });*/
            }
        }
    }

    private class MessageOutViewHolder extends RecyclerView.ViewHolder {

        TextView messageTV,dateTV;
        ImageView image;
        MessageOutViewHolder(final View itemView) {
            super(itemView);
            messageTV = itemView.findViewById(R.id.text_message_body);
            dateTV = itemView.findViewById(R.id.text_message_time);
            image = itemView.findViewById(R.id.ivPlay);
        }
        void bind(int position) {
            MessageModel.DataBean messageModel = list.get(position);
            messageTV.setText(messageModel.getText());
            dateTV.setText(PriceFormatter.get12HrFormatTime(list.get(position).getCreated_at().substring(11,16))+" | "+PriceFormatter.formatDateMonnth(list.get(position).getCreated_at().substring(0,10)));
            image.setVisibility(View.GONE);
            messageTV.setVisibility(View.VISIBLE);
            dateTV.setVisibility(View.VISIBLE);
           if (messageModel.getMsg_type().equals("2")){
                image.setVisibility(View.VISIBLE);
                messageTV.setVisibility(View.GONE);
                dateTV.setVisibility(View.GONE);
              //  Glide.with(context).load(messageModel.getMessage()).into(image);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callbackk.onClickOnVoiceMessage(messageModel.getText());
                    }
                });
            }
          /*  else  if (messageModel.getMessage_type().equals("3")){
                image.setVisibility(View.VISIBLE);
                messageTV.setVisibility(View.GONE);
             //   Glide.with(context).load(messageModel.getMessage()).into(image);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                     //   context.startActivity(IntentHelper.getVideoplayer(context).putExtra("url",list.get(position).getMessage()));
                    }
                });
            }*/
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == MESSAGE_TYPE_IN) {
            return new MessageInViewHolder(LayoutInflater.from(context).inflate(R.layout.item_text_in, parent, false));
        }
        return new MessageOutViewHolder(LayoutInflater.from(context).inflate(R.layout.item_text_out, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (list.get(position).getIs_admin().equalsIgnoreCase("1")) {
            ((MessageInViewHolder) holder).bind(position);
        } else {
            ((MessageOutViewHolder) holder).bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position).getIs_admin().equalsIgnoreCase("1")){
            return 1;
        }else {
            return 2;
        }

    }
    public void addMessage(MessageModel.DataBean mesasage){
        list.add(mesasage);
        notifyItemInserted(list.size());
    }
    public void addAllMessage(ArrayList<MessageModel.DataBean> mesasage){
        list.clear();
        list.addAll(mesasage);
       notifyDataSetChanged();
    }
}