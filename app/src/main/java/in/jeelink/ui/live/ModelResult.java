package in.jeelink.ui.live;

import java.util.List;

public class ModelResult {


    /**
     * status : true
     * message : successfully.
     * data : [{"id":"1","question_name":"test","option_1":"1","option_2":"2","option_3":"3","option_4":"4","right_option":"1","q_time":"20","type":"text","per_op1":0,"per_op2":100,"per_op3":0,"per_op4":0,"created_at":"2021-04-27 06:23:29","is_right":"0","your_answer":"2"}]
     */

    private boolean status;
    private String message;
    /**
     * id : 1
     * question_name : test
     * option_1 : 1
     * option_2 : 2
     * option_3 : 3
     * option_4 : 4
     * right_option : 1
     * q_time : 20
     * type : text
     * per_op1 : 0
     * per_op2 : 100
     * per_op3 : 0
     * per_op4 : 0
     * created_at : 2021-04-27 06:23:29
     * is_right : 0
     * your_answer : 2
     */

    private List<DataBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String id;
        private String question_name;
        private String option_1;
        private String option_2;
        private String option_3;
        private String option_4;
        private String right_option;
        private String q_time;
        private String type;
        private int per_op1;
        private int per_op2;
        private int per_op3;
        private int per_op4;
        private String created_at;
        private String is_right;
        private String your_answer;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getQuestion_name() {
            return question_name;
        }

        public void setQuestion_name(String question_name) {
            this.question_name = question_name;
        }

        public String getOption_1() {
            return option_1;
        }

        public void setOption_1(String option_1) {
            this.option_1 = option_1;
        }

        public String getOption_2() {
            return option_2;
        }

        public void setOption_2(String option_2) {
            this.option_2 = option_2;
        }

        public String getOption_3() {
            return option_3;
        }

        public void setOption_3(String option_3) {
            this.option_3 = option_3;
        }

        public String getOption_4() {
            return option_4;
        }

        public void setOption_4(String option_4) {
            this.option_4 = option_4;
        }

        public String getRight_option() {
            return right_option;
        }

        public void setRight_option(String right_option) {
            this.right_option = right_option;
        }

        public String getQ_time() {
            return q_time;
        }

        public void setQ_time(String q_time) {
            this.q_time = q_time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getPer_op1() {
            return per_op1;
        }

        public void setPer_op1(int per_op1) {
            this.per_op1 = per_op1;
        }

        public int getPer_op2() {
            return per_op2;
        }

        public void setPer_op2(int per_op2) {
            this.per_op2 = per_op2;
        }

        public int getPer_op3() {
            return per_op3;
        }

        public void setPer_op3(int per_op3) {
            this.per_op3 = per_op3;
        }

        public int getPer_op4() {
            return per_op4;
        }

        public void setPer_op4(int per_op4) {
            this.per_op4 = per_op4;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getIs_right() {
            return is_right;
        }

        public void setIs_right(String is_right) {
            this.is_right = is_right;
        }

        public String getYour_answer() {
            return your_answer;
        }

        public void setYour_answer(String your_answer) {
            this.your_answer = your_answer;
        }
    }
}
