package in.jeelink.ui.live;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.eacademy.R;
import com.eacademy.databinding.DialogPdfViewBinding;
import com.eacademy.databinding.DialogSendDialogBinding;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import in.jeelink.model.modellogin.ModelLogin;
import in.jeelink.ui.live.ModelQuestion;
import in.jeelink.utils.AppConsts;
import in.jeelink.utils.ProjectUtils;
import in.jeelink.utils.sharedpref.SharedPref;
public class DialogPdfView implements View.OnClickListener {
    Dialog dialog;
    private String url;

    DialogPdfViewBinding binding;
    private Context context;


    public DialogPdfView(Context context, String url) {
        this.context = context;
        dialog = new Dialog(context);

        this.url = url;
    }

    public void showDialog() {

        binding = DataBindingUtil.bind(LayoutInflater.from(context).inflate(R.layout.dialog_pdf_view, null));
        dialog.setContentView(binding.getRoot());
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 1);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        dialog.setCancelable(true);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.getWindow().setLayout(width, height);
        dialog.setCancelable(false);


        binding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });

        if (!((Activity) context).isFinishing()) {
            dialog.show();
            //show dialog
        }


    }

    public void dismissDialog() {
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {

    }
    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }
}



