package in.jeelink.ui.live;

import java.util.List;

public class ModelQuestion {

    /**
     * data : [{"batch":"2","created_at":"2021-04-27 06:10:13","date":"2021-04-27","id":"1","live_id":"3","option_1":"1","option_2":"2","option_3":"3","option_4":"4","q_time":"20","question_name":"test","right_option":"1","time":"11:40:00","type":"text"}]
     * message : successfully.
     * status : true
     */

    private String message;
    private boolean status;
    /**
     * batch : 2
     * created_at : 2021-04-27 06:10:13
     * date : 2021-04-27
     * id : 1
     * live_id : 3
     * option_1 : 1
     * option_2 : 2
     * option_3 : 3
     * option_4 : 4
     * q_time : 20
     * question_name : test
     * right_option : 1
     * time : 11:40:00
     * type : text
     */

    private List<DataBean> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String batch;
        private String created_at;
        private String date;
        private String id;
        private String live_id;
        private String option_1;
        private String option_2;
        private String option_3;
        private String option_4;
        private String q_time;
        private String question_name;
        private String right_option;
        private String time;
        private String type;

        public String getBatch() {
            return batch;
        }

        public void setBatch(String batch) {
            this.batch = batch;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLive_id() {
            return live_id;
        }

        public void setLive_id(String live_id) {
            this.live_id = live_id;
        }

        public String getOption_1() {
            return option_1;
        }

        public void setOption_1(String option_1) {
            this.option_1 = option_1;
        }

        public String getOption_2() {
            return option_2;
        }

        public void setOption_2(String option_2) {
            this.option_2 = option_2;
        }

        public String getOption_3() {
            return option_3;
        }

        public void setOption_3(String option_3) {
            this.option_3 = option_3;
        }

        public String getOption_4() {
            return option_4;
        }

        public void setOption_4(String option_4) {
            this.option_4 = option_4;
        }

        public String getQ_time() {
            return q_time;
        }

        public void setQ_time(String q_time) {
            this.q_time = q_time;
        }

        public String getQuestion_name() {
            return question_name;
        }

        public void setQuestion_name(String question_name) {
            this.question_name = question_name;
        }

        public String getRight_option() {
            return right_option;
        }

        public void setRight_option(String right_option) {
            this.right_option = right_option;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
