package in.jeelink.ui.live;

import java.util.List;

public class ModelLiveClass {

    /**
     * status : true
     * message : successfully.
     * data : [{"id":"1","class_name":"test","chanl_id":"@PXRLTJMG4647","teacher_id":"1","created_at":"2021-03-13 10:06:29"},{"id":"2","class_name":"ok","chanl_id":"@FZYJGWLV7265","teacher_id":"1","created_at":"2021-03-13 10:10:10"},{"id":"3","class_name":"ok","chanl_id":"@EIYSQWCZ5681","teacher_id":"1","created_at":"2021-03-13 10:50:11"},{"id":"4","class_name":"ok","chanl_id":"@ONWDAGSE8811","teacher_id":"1","created_at":"2021-03-13 11:24:27"},{"id":"5","class_name":"class One","chanl_id":"@AHNCYVXK4037","teacher_id":"3","created_at":"2021-03-13 11:37:05"},{"id":"6","class_name":"asd","chanl_id":"@NBCHREKI3605","teacher_id":"3","created_at":"2021-03-13 11:40:07"}]
     */

    private boolean status;
    private String message;
    /**
     * id : 1
     * class_name : test
     * chanl_id : @PXRLTJMG4647
     * teacher_id : 1
     * created_at : 2021-03-13 10:06:29
     */

    private List<DataBean> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String id;
        private String class_name;
        private String chanl_id;
        private String teacher_id;
        private String created_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getClass_name() {
            return class_name;
        }

        public void setClass_name(String class_name) {
            this.class_name = class_name;
        }

        public String getChanl_id() {
            return chanl_id;
        }

        public void setChanl_id(String chanl_id) {
            this.chanl_id = chanl_id;
        }

        public String getTeacher_id() {
            return teacher_id;
        }

        public void setTeacher_id(String teacher_id) {
            this.teacher_id = teacher_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
