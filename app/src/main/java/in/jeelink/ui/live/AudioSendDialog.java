package in.jeelink.ui.live;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.eacademy.R;
import com.eacademy.databinding.DialogSendDialogBinding;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import in.jeelink.model.modellogin.ModelLogin;
import in.jeelink.ui.live.ModelQuestion;
import in.jeelink.utils.AppConsts;
import in.jeelink.utils.ProjectUtils;
import in.jeelink.utils.sharedpref.SharedPref;
public class AudioSendDialog implements View.OnClickListener {
    Dialog dialog;
    CountDownTimer countDownTimer;
    int selectedOption=0;
    DialogSendDialogBinding binding;
    private Context context;
    private ModelLogin modelLogin;
    private Callbackk callbackk;
    private MediaRecorder recorder;
    File audioFile;
    private MediaPlayer player;
    private String fileName;
    private final int AUDIO_NOT_STARTED=1;
    private final int AUDIO_RECORDING=2;
    private final int AUDIO_PLAYING=3;
    private final int AUDIO_STOP=4;


    public AudioSendDialog(Context context,ModelLogin model, Callbackk callbackk) {
        this.context = context;
        dialog = new Dialog(context);
        modelLogin = model;
        this.callbackk = callbackk;
    }
    public void showDialog() {

        binding = DataBindingUtil.bind(LayoutInflater.from(context).inflate(R.layout.dialog_send_dialog, null));
        dialog.setContentView(binding.getRoot());
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 1);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        dialog.setCancelable(true);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.getWindow().setLayout(width, height);
        dialog.setCancelable(false);
  
        if(!((Activity) context).isFinishing())
        {
            dialog.show();
            //show dialog
        }


        binding.activityMainRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // startRecording();
            }
        });


        binding.activityMainStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopRecording();
            }
        });


        binding.activityMainPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playRecording();
            }
        });


        binding.activityMainStopPlaying.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopPlaying();
            }
        });
        binding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //     playingStatus(AUDIO_STOP);
                dismissDialog();
                callbackk.onClickOnMute();
            }
        });
        binding.buttonSendAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if (fileName!=null){
                    stopRecording();
                    sendAudioMessage();

                }else {
                    dismissDialog();
                    makeToast("Please Add Voice first");
                }*/
                callbackk.onClickOnMute();
                dismissDialog();

            }
        });





    }

    public void dismissDialog(){
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {

    }


    public interface  Callbackk{
        void onQuestionRaisedSuccessfully(Boolean isSuccessfull,String fileName);
        void onPlaying(Boolean isAudioPlaying);
        void  onClickOnMute();
    }


    void playingStatus(int status){
        switch (status){

            case AUDIO_NOT_STARTED:
                binding.activityMainPlay.setVisibility(View.GONE);
                binding.activityMainRecord.setVisibility(View.VISIBLE);
                binding.activityMainStop.setVisibility(View.GONE);
                callbackk.onPlaying(false);
                break;
            case AUDIO_PLAYING:
                binding.activityMainPlay.setVisibility(View.VISIBLE);
                binding.activityMainRecord.setVisibility(View.GONE);
                binding.activityMainStop.setVisibility(View.GONE);
                callbackk.onPlaying(true);
                break;
            case AUDIO_RECORDING:
                binding.activityMainPlay.setVisibility(View.GONE);
                binding.activityMainRecord.setVisibility(View.GONE);
                binding.activityMainStop.setVisibility(View.VISIBLE);
                callbackk.onPlaying(true);
                break;
            case AUDIO_STOP:
                binding.activityMainPlay.setVisibility(View.VISIBLE);
                binding.activityMainRecord.setVisibility(View.GONE);
                binding.activityMainStop.setVisibility(View.GONE);
                callbackk.onPlaying(false);
                break;


        }
    }

    void sendAudioMessage() {
        AndroidNetworking.upload(AppConsts.BASE_URL + AppConsts.API_SEND_AUDIO)
                .addMultipartFile("myfile", (new File(fileName)))
                .addMultipartParameter(AppConsts.STUDENT_ID, modelLogin.getStudentData().getStudentId())
                .addMultipartParameter(AppConsts.BATCH_ID, modelLogin.getStudentData().getBatchId())
                .addMultipartParameter("msg_type","2")
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        dismissDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                              callbackk.onQuestionRaisedSuccessfully(true,fileName);
                            } else {
                               Toast.makeText(context, context.getResources().getString(R.string.Try_again), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            dismissDialog();
                        }
                        ProjectUtils.pauseProgressDialog();
                    }

                    @Override
                    public void onError(ANError anError) {

                        dismissDialog();
                        ProjectUtils.pauseProgressDialog();
                    }
                });
    }


    private void makeToast(String message) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    private void startRecording() {
        playingStatus(AUDIO_RECORDING);
        String uuid = UUID.randomUUID().toString();
        fileName = context.getFilesDir().getPath() + "/" + uuid + ".mp3";

        ContentValues values = new ContentValues(3);
        values.put(MediaStore.MediaColumns.TITLE, fileName);
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(fileName);
           recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
    //    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
         Log.i("asddad", fileName);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(context.getClass().getSimpleName() + ":startRecording()", "prepare() failed");
        }

        recorder.start();

        // startRecorderService();
    }

    private void stopRecording() {
        if (recorder != null) {
            recorder.release();
            recorder = null;
            Log.d("asdasd","asdsad");
            playingStatus(AUDIO_STOP);
            // stopRecorderService();
        }
    }

    private void playRecording() {
        playingStatus(AUDIO_PLAYING);
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopPlaying();
                    callbackk.onPlaying(false);
                }
            });
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.e(  ":playRecording()", "prepare() failed");
        }
    }

    private void stopPlaying() {
        if (player != null) {
            player.release();
            player = null;
            playingStatus(AUDIO_STOP);
        }
    }

}
