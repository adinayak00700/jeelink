package in.jeelink.ui.live;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;


import com.eacademy.R;
import com.eacademy.databinding.QuestionResultDialogBinding;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import in.jeelink.ui.academicrecord.ActivityAcademicRecord;


public class BottomSheetDialogResult {
    BottomSheetDialog bottomSheetDialog;
    private Context context;
    private ModelResult.DataBean modelResult;
    QuestionResultDialogBinding binding;

    public BottomSheetDialogResult(Context context, ModelResult.DataBean modelResult){
        this.context = context;
        this.modelResult = modelResult;
    }

    public void showDialog(){
        bottomSheetDialog=new BottomSheetDialog(context);
        View view= LayoutInflater.from(context).inflate(R.layout.question_result_dialog,null);
        binding= DataBindingUtil.bind(view);
        bottomSheetDialog.setContentView(binding.getRoot());

        binding.buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });
        if(!((Activity) context).isFinishing())
        {
            bottomSheetDialog.show();
        }


        ArrayList<BarDataSet> dataSets = null;
        List<IBarDataSet> bars = new ArrayList<>();
        ArrayList<BarEntry> entries1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(0,Float.parseFloat(String.valueOf(modelResult.getPer_op1())) ); // Jan
        entries1.add(v1e1);
        BarEntry v1e2 = new BarEntry(1,Float.parseFloat(String.valueOf(modelResult.getPer_op2())) ); // Feb
        entries1.add(v1e2);
        BarEntry v1e3 = new BarEntry(2,Float.parseFloat(String.valueOf(modelResult.getPer_op3())) ); // Mar
        entries1.add(v1e3);
        BarEntry v1e4 = new BarEntry(3,Float.parseFloat(String.valueOf(modelResult.getPer_op4())) ); // Apr
        entries1.add(v1e4);

        BarDataSet barDataSet1 = new BarDataSet(entries1, "label");

        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        bars.add(barDataSet1);


        BarData data = new BarData(bars);
        binding.chart.setData(data);
        data.setValueFormatter(new ActivityAcademicRecord.MyValueFormatter());
        data.setValueTextSize(14f);
        Legend l = binding.chart.getLegend();
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        setupchart();
        binding.tvLabelQuestion.setText(modelResult.getQuestion_name());
        if (modelResult.getIs_right().equals("1")){
            binding.tvYourAnswerwas.setText("Your Answer was Right ");
            binding.tvYourAnswerwas.setTextColor(ContextCompat.getColor(context,R.color.color_blue));
            binding.tvRightAnswer.setVisibility(View.GONE);
        }else {
            binding.tvYourAnswerwas.setText("Your Answer was Wrong ");
            binding.tvYourAnswerwas.setTextColor(ContextCompat.getColor(context,R.color.color_blue));
            binding.tvRightAnswer.setVisibility(View.VISIBLE);

            switch (Integer.parseInt(modelResult.getRight_option())){
                case 1:
                    binding.tvRightAnswer.setText("Right option was A");
                    break;
                case  2:
                    binding.tvRightAnswer.setText("Right option was B");
                    break;
                case 3:
                    binding.tvRightAnswer.setText("Right option was C");
                    break;
                case 4:
                    binding.tvRightAnswer.setText("Right option was D");
                    break;
            }

        }

    }


   void setupchart(){

       binding.chart.setDescription(null);
       binding.chart.getDrawingCache(false);
       binding.chart.getLegend().setEnabled(false);
       binding.chart.getAxisLeft().setDrawTopYLabelEntry(false);
       binding.chart.getAxisLeft().setDrawTopYLabelEntry(false);
       binding.chart.getAxisLeft().setDrawLimitLinesBehindData(false);
       binding.chart.getAxisLeft().setDrawAxisLine(false);
     /*  binding.chart.setDrawValuesForWholeStack(false);*/
       binding.chart.getAxisLeft().setDrawGridLines(false);
       binding.chart.getAxisLeft().setEnabled(false);
       binding.chart.getAxisRight().setEnabled(false);

       binding.chart.getAxisRight().setDrawLimitLinesBehindData(false);
       binding.chart.getAxisRight().setDrawAxisLine(false);
       binding.chart.getAxisRight().setDrawGridLines(false);
       binding.chart.getAxisRight().setDrawTopYLabelEntry(false);

       binding.chart.animateXY(2000, 2000);
       binding.chart.getXAxis().setDrawLabels(true);
       XAxis xAxis = binding.chart.getXAxis();
       xAxis.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);

       ValueFormatter formatter = new ValueFormatter() {


           @Override
           public String getFormattedValue(float value) {
               return getXAxisValues().get((int) value);
           }
       };

       xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
       xAxis.setValueFormatter(formatter);
       binding.chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
       binding.chart.getXAxis().setDrawGridLines(false);
       binding.chart.getXAxis().setDrawAxisLine(false);


       binding.chart.invalidate();



    }


    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("A");
        xAxis.add("B");
        xAxis.add("C");
        xAxis.add("D");
        return xAxis;
    }

    public class MyValueFormatter extends ValueFormatter {

        private DecimalFormat mFormat;

        public MyValueFormatter() {
            mFormat = new DecimalFormat("#");
        }

        @Override
        public String getFormattedValue(float value) {
            return mFormat.format(value)+"%";
        }
    }



    void makeToast(String  message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }


}
