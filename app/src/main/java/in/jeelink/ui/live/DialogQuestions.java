package in.jeelink.ui.live;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.bumptech.glide.Glide;
import com.eacademy.R;
import com.eacademy.databinding.DialogQuestionsBinding;
import com.google.gson.Gson;
import com.zipow.videobox.util.GlideRequests;

import java.util.HashMap;

import in.jeelink.ui.live.ModelQuestion;
import in.jeelink.utils.AppConsts;
import in.jeelink.utils.ProjectUtils;
import in.jeelink.utils.sharedpref.SharedPref;


public class DialogQuestions implements View.OnClickListener {
    Dialog dialog;
    CountDownTimer countDownTimer;
    int selectedOption=0;
    DialogQuestionsBinding binding;
    private Context context;
    private ModelQuestion.DataBean model;
    private Callbackk callbackk;
    DialogPdfView dialogPdfView;
    private String channelId;

    public DialogQuestions(Context context) {
        this.context = context;
        dialog = new Dialog(context);
    }
    public void showDialog(ModelQuestion.DataBean model,Callbackk callbackk,String channelId) {
        this.model = model;

        this.callbackk = callbackk;
        this.channelId = channelId;
        binding = DataBindingUtil.bind(LayoutInflater.from(context).inflate(R.layout.dialog_questions, null));
        dialog.setContentView(binding.getRoot());
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 1);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        dialog.setCancelable(true);
         callbackk.onAutoShowResults((Integer.parseInt(model.getQ_time())+2));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.getWindow().setLayout(width, height);
       startCountDwn((Integer.parseInt(model.getQ_time())*1000));
        binding.tvQuestion.setText(model.getQuestion_name());
        binding.TextViewOp1.setText(model.getOption_1());
        binding.TextViewOp2.setText(model.getOption_2());
        binding.TextViewOp3.setText(model.getOption_3());
        binding.TextViewOp4.setText(model.getOption_4());
        binding.buttonClose.setOnClickListener(this);
        binding.buttonOk.setOnClickListener(this);
      /* MCQAdapter adapter=new MCQAdapter(context);
       binding.listview.setAdapter(adapter);*/
    /*    new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },250);*/
        binding.TextViewOp1.setOnClickListener(this);
        binding.TextViewOp2.setOnClickListener(this);
        binding.TextViewOp3.setOnClickListener(this);
        binding.TextViewOp4.setOnClickListener(this);


        if(!((Activity) context).isFinishing())
        {
            if (dialog!=null){
                dialog.show();
            }

        }

        binding.tvReloadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPdf();
            }
        });

        if (model.getType().equals("file")){
            binding.tvQuestion.setVisibility(View.GONE);
            binding.ivQuestion.setVisibility(View.VISIBLE);
            binding.webviewPdf.setVisibility(View.GONE);
            try {
                Glide.with(context).load(model.getQuestion_name()).error(R.drawable.ic_selected_circle_indicator).into(binding.ivQuestion);

            }catch (Exception e){
                Log.d("asdasda","asdasd");
            }
       }
        else if (model.getType().equals("pdf")){
      showPdf();
        }

    }

    private void showPdf() {
        binding.tvQuestion.setVisibility(View.GONE);
        binding.ivQuestion.setVisibility(View.GONE);
        binding.webviewPdf.setVisibility(View.VISIBLE);
        try {
            binding.webviewPdf.clearCache(true);
            binding.webviewPdf.clearFormData();
            binding.webviewPdf.clearHistory();
            binding.webviewPdf.getSettings().setJavaScriptEnabled(true);
            binding.webviewPdf.getSettings().setPluginState(WebSettings.PluginState.ON);
            //---you need this to prevent the webview from
            // launching another browser when a url
            // redirection occurs---
            binding.webviewPdf.setWebViewClient(new Callback());


            binding.webviewPdf.loadUrl(
                    "http://docs.google.com/gview?embedded=true&url=" + model.getQuestion_name());
        }catch (Exception e){
            Log.d("asdasda","asdasd");
        }
        
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }

    public void dismissDialog(){
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.TextViewOp1:
                selectedOption=1;
                binding.TextViewOp1.setTextColor(ContextCompat.getColor(context,R.color.white));
                binding.TextViewOp2.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp3.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp4.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));


                binding.TextViewOp1.setBackground(ContextCompat.getDrawable(context,R.drawable.small_purple_round_corner));
                binding.TextViewOp2.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp3.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp4.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));

                break;
            case R.id.TextViewOp2:
                selectedOption=2;
                binding.TextViewOp1.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp2.setTextColor(ContextCompat.getColor(context,R.color.white));
                binding.TextViewOp3.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp4.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));


                binding.TextViewOp1.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp2.setBackground(ContextCompat.getDrawable(context,R.drawable.small_purple_round_corner));
                binding.TextViewOp3.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp4.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));

                break;
            case R.id.TextViewOp3:
                selectedOption=3;
                binding.TextViewOp1.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp2.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp3.setTextColor(ContextCompat.getColor(context,R.color.white));
                binding.TextViewOp4.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));


                binding.TextViewOp1.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp2.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp3.setBackground(ContextCompat.getDrawable(context,R.drawable.small_purple_round_corner));
                binding.TextViewOp4.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));

                break;
            case R.id.TextViewOp4:
                selectedOption=4;
                binding.TextViewOp1.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp2.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp3.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                binding.TextViewOp4.setTextColor(ContextCompat.getColor(context,R.color.white));

                binding.TextViewOp1.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp2.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp3.setBackground(ContextCompat.getDrawable(context,R.drawable.small_white_round_corner));
                binding.TextViewOp4.setBackground(ContextCompat.getDrawable(context,R.drawable.small_purple_round_corner));

                break;
            case R.id.buttonClose:
                countDownTimer.cancel();
                dismissDialog();
                break;
            case R.id.buttonOk:
                if (selectedOption!=0){
                    binding.buttonOk.setOnClickListener(null);
                    submitAnswer();
                }else {
                    makeToast("Select a option");
                }

                break;

        }
    }
    public void startCountDwn(long milliseconds){
        countDownTimer=   new CountDownTimer(milliseconds, 1000) {

            public void onTick(long millisUntilFinished) {
                binding.tvTimeLeftValue.setText("Time Left: " + millisUntilFinished / 1000);

            }

            public void onFinish() {
                binding.tvTimeLeftValue.setText("Over");

         /*       if (dialogPdfView!=null){
                    dialogPdfView.dismissDialog();
                }*/

                dialog.dismiss();
            }

        }.start();
    }

    public interface  Callbackk{
        void onAutoShowResults(int duration);
    }

    void submitAnswer() {
        ProjectUtils.showProgressDialog(context, true, context.getResources().getString(R.string.Loading___));
        HashMap<String,String> hashMap=new HashMap<>();
       hashMap.put(AppConsts.STUDENT_ID, new SharedPref().getUser(AppConsts.STUDENT_DATA).getStudentData().getStudentId());
                hashMap.put(AppConsts.QUESTION_ID, model.getId());
                hashMap.put("answer",String.valueOf(selectedOption));
        AndroidNetworking.post(AppConsts.BASE_URL+AppConsts.API_SUBMIT_ANSWER)
                .addStringBody(new Gson().toJson(hashMap))
                .setTag(AppConsts.API_PRACTICE_TEST_RESULT)
                .build()
                .getAsObject(ModelSuccess.class, new ParsedRequestListener<ModelSuccess>() {


                    @Override
                    public void onResponse(ModelSuccess response) {
                        ProjectUtils.pauseProgressDialog();
                        if (response.isStatus()){
                            if (response!=null){
                                    if (response.isStatus()){
                                        countDownTimer.cancel();
                                        dialog.dismiss();
                                    }else {
                                        dialog.dismiss();

                                }
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        ProjectUtils.pauseProgressDialog();
                    }
                });
    }


    private void makeToast(String message) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
}
