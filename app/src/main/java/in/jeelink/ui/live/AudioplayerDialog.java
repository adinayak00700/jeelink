package in.jeelink.ui.live;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.TimedText;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.eacademy.R;
import com.eacademy.databinding.AudioPlayerDialogBinding;
import com.eacademy.databinding.DialogSendDialogBinding;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import in.jeelink.model.modellogin.ModelLogin;
import in.jeelink.ui.live.ModelQuestion;
import in.jeelink.utils.AppConsts;
import in.jeelink.utils.ProjectUtils;
import in.jeelink.utils.sharedpref.SharedPref;
public class AudioplayerDialog implements View.OnClickListener {
    Dialog dialog;
    private Callbackk callbackk;
    MediaPlayer player = new MediaPlayer();
    AudioPlayerDialogBinding binding;
    private Context context;
    ProgressBar progressBar;
    TextView textCounter;

    MyCountDownTimer myCountDownTimer;

    private String audioPath;


    public AudioplayerDialog(Context context,Callbackk callbackk) {
        this.context = context;
        dialog = new Dialog(context);
        this.callbackk = callbackk;
    }

    public interface Callbackk{
        void isAudioPlaying(Boolean isPlaying);
    }

    public void showDialog(String audioPath) {
        this.audioPath = audioPath;


        binding = DataBindingUtil.bind(LayoutInflater.from(context).inflate(R.layout.audio_player_dialog, null));
        dialog.setContentView(binding.getRoot());
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 1);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        dialog.setCancelable(true);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        dialog.getWindow().setLayout(width, height);
        dialog.setCancelable(false);


        if(!((Activity) context).isFinishing())
        {
            dialog.show();
            //show dialog
        }
        try {
            playRecording(audioPath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        binding.ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbackk.isAudioPlaying(false);
                dismissDialog();
            }
        });


    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int progress = (int) (millisUntilFinished/100);
            progressBar.setProgress(progress);
        }

        @Override
        public void onFinish() {
            progressBar.setProgress(0);
        }

    }

    public void dismissDialog(){
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {

    }



    private void makeToast(String message) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }



    private void playRecording(String path) {

        player = new MediaPlayer();
        try {
            player.setDataSource(path);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopPlaying();

                }
            });
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                   player.start();
                    setUpProgress();
                    Log.e(  ":playRecosdrding()", "prepare() failed");
                }
            });


            player.prepare();




        } catch (IOException e) {
            Log.e(  ":playRecording()", "prepare() failed");
        }
    }







    private void setUpProgress() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after .250 seconds
                if (player!=null){
                    float progress = ((float) player.getCurrentPosition() / player.getDuration()) * 100;
                    Log.d("asdasdasd",String.valueOf(player.getDuration()));
                    binding.progressbar.setProgress((int) progress);
                    handler.postDelayed(this, 250);
                }

            }
        }, 250);  //the time is in miliseconds


    }

    private void stopPlaying() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

}
