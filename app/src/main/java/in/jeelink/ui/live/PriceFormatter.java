package in.jeelink.ui.live;

import android.app.Activity;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PriceFormatter {

    public static String changeDateToTime(String serverdate) {
        if (serverdate==null){
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(serverdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.format(d);
    }

    public static String visaDateFormat(String serverDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(serverDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }

    public static String dobFormat(String serverDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(serverDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }


    public static String formatYMD(String serverDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(serverDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }


    public static String formatDMY(String serverDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(serverDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }

    public static String formatDateMonnth(String serverDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("dd-MMM", Locale.US);
        Date d = null;
        try {
            d = sdf.parse(serverDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }



    public static String changeDateFormateYMD(String serverdate) {
        if (serverdate==null){
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("en"));
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));

        Date d = null;
        try {
            d = sdf.parse(serverdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);
    }


    public static String get12HrFormatTime(String time)
    {
        String convertedTime ="";
        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
            SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
            Date date = parseFormat.parse(time);
            convertedTime=displayFormat.format(date);
            System.out.println("convertedTime : "+convertedTime);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
        //Output will be 10:23 PM
    }








    public static String formatMonthAndYear(String time) {
        if (time==null){
            return "";
        }
        Date dateObj = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",new Locale("en"));
            dateObj = sdf.parse(time);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return new SimpleDateFormat("MMMM dd, yyyy",new Locale("en")).format(dateObj);
    }




    public static int getDrawerDragDistance(Context context){
        int totalWidthInDp = (int)dpFromPx(context, context.getResources().getDisplayMetrics().widthPixels);
        double widthForContentView = totalWidthInDp*0.35;
        return totalWidthInDp-(int)widthForContentView;
    }

    private static float dpFromPx(Context context, float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    private static int getModolus(int min){
        int mod = (min + 30)%60;
        return (min + 30) - mod;
    }

    public static String getTime(String time){

        String parts[] = time.split(":");
        int min = Integer.parseInt(parts[1]);
        int hours = Integer.parseInt(parts[0]);

        if (getModolus(min) == 0){
            int h = hours + 1;
            int m = 30;
            return h+":"+m;
        }else if (getModolus(min) == 60){
            int h = hours + 2;
            int m = 0;
            return h+":00";
        }else {
            return "00:00";
        }
    }

    public static int startCalValidation(String time){
        String parts[] = time.split(":");
        int hours = Integer.parseInt(parts[0]);
        int min = Integer.parseInt(parts[1]);
        if (hours>22){
            return 0;
        }else {
            return 0;
        }

    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {

        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public static String customDateFormatAMPM(String timeIN24Hr) throws Exception {
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        Date date = parseFormat.parse(timeIN24Hr);
        return displayFormat.format(date);

    }




}
