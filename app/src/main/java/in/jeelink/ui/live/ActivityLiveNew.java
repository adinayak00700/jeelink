package in.jeelink.ui.live;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.eacademy.R;
import com.eacademy.databinding.ActivityLiveNewBinding;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.jeelink.model.attendanceModel.ModelAttendance;
import in.jeelink.model.modelLeave.ModelLeaveHistory;
import in.jeelink.model.modellogin.ModelLogin;
import in.jeelink.ui.applyleave.ActivityApplyLeave;
import in.jeelink.ui.attendance.ActivityAttendance;
import in.jeelink.utils.AppConsts;
import in.jeelink.utils.ProjectUtils;
import in.jeelink.utils.sharedpref.SharedPref;
import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import okhttp3.OkHttpClient;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;

public class ActivityLiveNew extends AppCompatActivity implements View.OnClickListener, CustomChatAdapter.Callbackk {
    DatabaseReference mDatabase;
    Boolean isFirstTime = true;
    Boolean isFirstTimeChatApiLods = true;
    Boolean isFirstTimeKeyListener = true;
    AudioSendDialog audioSendDialog;
    DialogQuestions dialogQuestions;
    String passwordMeeting, numberMeeting, sdkKey, secretKey, youtube_url;
    SharedPref sharedPref;

    ModelLogin modelLogin;
    ActivityLiveNewBinding binding;
    CustomChatAdapter chatAdapter = new CustomChatAdapter(this, this);
    Parcelable recyclerViewState;

    YouTubePlayerView youTubePlayerView = null;
    YouTubePlayer youTubePlayerr;
    float currentDuration;
    Boolean isJustopen = true;
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static final String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private boolean audioRecordingPermissionGranted = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_live_new);

        sharedPref = SharedPref.getInstance(this);
        modelLogin = sharedPref.getUser(AppConsts.STUDENT_DATA);
        mDatabase = FirebaseDatabase.getInstance().getReference("meetings");


        if (getIntent().hasExtra("meetingPassword")) {
            passwordMeeting = getIntent().getStringExtra("meetingPassword");
            numberMeeting = getIntent().getStringExtra("meetingId");
            sdkKey = getIntent().getStringExtra("sdkKey");
            secretKey = getIntent().getStringExtra("sdkSecret");
            youtube_url = getIntent().getStringExtra("youtube_url");
        } else {
            apiMeetingData();
        }
        binding.ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.msgEdittext.getText().toString() != null) {
                    sendMessage(binding.msgEdittext.getText().toString());
                    binding.msgEdittext.setText("");
                } else {
                    makeToast("Enter Message");
                }
            }
        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(chatAdapter);
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {
                                      getChat();
                                  }

                              },
                0,
                3000);
        getQuestionListener();
        joinStudent(modelLogin.getStudentData().getStudentId());
        //  activateChatListener();
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                recyclerViewState = binding.recyclerView.getLayoutManager().onSaveInstanceState();
            }
        });

        binding.ivHandDown.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                handRaise(true, modelLogin.getStudentData().getStudentId());
            }
        });
        binding.ivHandRaise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handRaise(false, modelLogin.getStudentData().getStudentId());
            }
        });
        binding.ivCommenthide.setOnClickListener(this);
        binding.ivCommentShow.setOnClickListener(this);
        initVideoPlayer();
        activateHandKeyListener();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
        }else {
            initAgoraEngineAndJoinChannel();
        }

    }




    void initViews(){

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                audioRecordingPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                openAudioDialog();
                break;
        }

        if (!audioRecordingPermissionGranted) {
            finish();
        }
    }
    private void closeAudioDialog(){
        if (audioSendDialog!=null){
            audioSendDialog.dismissDialog();
        }

    }

    private void openAudioDialog() {
         audioSendDialog = new AudioSendDialog(ActivityLiveNew.this,modelLogin,new AudioSendDialog.Callbackk() {


            @Override
            public void onQuestionRaisedSuccessfully(Boolean isSuccessfull, String fileName) {
                if (chatAdapter.list.size() > 0) {
                    binding.recyclerView.smoothScrollToPosition(chatAdapter.list.size() - 1);
                }
            }

            @Override
            public void onPlaying(Boolean isAudioPlaying) {
                if (youTubePlayerr!=null){
                    if (isAudioPlaying) {
                        youTubePlayerr.mute();
                    } else {
                        youTubePlayerr.unMute();
                    }
                }

            }

            @Override
            public void onClickOnMute() {
                leaveChannel();
            }
        });
        audioSendDialog.showDialog();
    }


    void hideComments(Boolean isHide) {
        if (isHide) {
            binding.ivCommentShow.setVisibility(View.VISIBLE);
            binding.ivCommenthide.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.GONE);

        } else {
            binding.ivCommenthide.setVisibility(View.VISIBLE);
            binding.ivCommentShow.setVisibility(View.GONE);
            binding.recyclerView.setVisibility(View.VISIBLE);

        }

    }


    void initVideoPlayer() {
        youTubePlayerView = findViewById(R.id.videoPlayer);
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.enterFullScreen();
        youTubePlayerView.getPlayerUiController();
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                // loading the selected video into the YouTube Player
                youTubePlayerr = youTubePlayer;
                String sampleUrl = youtube_url;


                try {
                   /* String video_id = getYouTubeId(sampleUrl);
                    Log.d("asdVideourl",youtube_url);*/
                    youTubePlayer.loadVideo(sampleUrl, 0);
                    youTubePlayer.play();
                } catch (Exception e) {
                    Toast.makeText(ActivityLiveNew.this, "Not Valid Video ID", Toast.LENGTH_SHORT).show();
                }

                /**/
            }

            @Override
            public void onStateChange(@NonNull YouTubePlayer youTubePlayer,
                                      @NonNull PlayerConstants.PlayerState state) {
                // this method is called if video has ended,
                super.onStateChange(youTubePlayer, state);

            }

            @Override
            public void onCurrentSecond(@NotNull YouTubePlayer youTubePlayer, float second) {
                super.onCurrentSecond(youTubePlayer, second);
                currentDuration = second;
            }

            @Override
            public void onVideoDuration(@NotNull YouTubePlayer youTubePlayer, float duration) {
                super.onVideoDuration(youTubePlayer, duration);
                if (currentDuration != 0.0) {
                    if (isJustopen) {
                        youTubePlayer.seekTo(currentDuration);
                        Log.d("asdasda", String.valueOf(currentDuration));
                        isJustopen = false;
                    }

                }

            }
        });
        //   youTubePlayerView.enterFullScreen();
        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {

            }

            @Override
            public void onYouTubePlayerExitFullScreen() {

            }
        });
        youTubePlayerView.enterFullScreen();
    }

    private String getYouTubeId(String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "error";
        }
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        savedInstanceState.putDouble("currentDuration", currentDuration);

        // etc.

        super.onSaveInstanceState(savedInstanceState);
    }

//onRestoreInstanceState

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        if (savedInstanceState.getDouble("currentDuration") != 0.0) {
            currentDuration = (float) savedInstanceState.getDouble("currentDuration");
        }



    }


    private void getQuestionListener() {
        mDatabase.child(modelLogin.getStudentData().getBatchId()).child("randomNumber").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("asdasdgg","asdasd");
                if (isFirstTime) {
                    isFirstTime = false;
                } else {
                    Log.d("asdasd","asdasd");
                    getQuestion();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void activateHandKeyListener() {

        mDatabase.child(modelLogin.getStudentData().getBatchId()).child("joinedStudents").child(modelLogin.getStudentData().getStudentId()).child(("isHandRaised")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (isFirstTimeKeyListener) {
                    isFirstTimeKeyListener = false;
                } else {
                    try {
                        switch (Integer.parseInt((String) dataSnapshot.getValue())) {
                            case 2:
                                if (ActivityCompat.checkSelfPermission(ActivityLiveNew.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

                                    ActivityCompat.requestPermissions(ActivityLiveNew.this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

                                } else {
                                    openAudioDialog();
                                    joinChannel();
                                    makeToast("Your Request has been Accepted by Teacher , Now you can ask your Query");
                                    binding.ivHandRaise.setVisibility(View.GONE);
                                    binding.ivHandDown.setVisibility(View.VISIBLE);
                                }
                                break;
                            case 3:
                                makeToast("Your Request has been rejected");
                                binding.ivHandRaise.setVisibility(View.GONE);
                                binding.ivHandDown.setVisibility(View.VISIBLE);

                                break;
                            case 0:
                                         leaveChannel();
                                closeAudioDialog();
                                binding.ivHandRaise.setVisibility(View.GONE);
                                binding.ivHandDown.setVisibility(View.VISIBLE);

                                break;
                            case 1:
                                binding.ivHandRaise.setVisibility(View.VISIBLE);
                                binding.ivHandDown.setVisibility(View.GONE);

                                break;
                        }
                    } catch (Exception e) {

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }


    private void joinStudent(String studentId) {
        Log.d("sadfasd",studentId);
        mDatabase.child(modelLogin.getStudentData().getBatchId()).child("joinedStudents").child(studentId).child("studentId").setValue(studentId);
        handRaise(false, modelLogin.getStudentData().getStudentId());

    }


    void apiMeetingData() {

        ProjectUtils.showProgressDialog(ActivityLiveNew.this, false, getResources().getString(R.string.Loading___));
        AndroidNetworking.post(AppConsts.BASE_URL + AppConsts.API_LIVE_CLASS_DATA)
                .addBodyParameter(AppConsts.BATCH_ID, modelLogin.getStudentData().getBatchId())
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        ProjectUtils.pauseProgressDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (AppConsts.TRUE.equals("" + jsonObject.getString("status"))) {
                                JSONArray jsonArray = new JSONArray("" + jsonObject.getString("data"));
                                JSONObject jsonObject1 = new JSONObject("" + jsonArray.get(0));

                                numberMeeting = "" + jsonObject1.getString("meetingId");
                                passwordMeeting = "" + jsonObject1.getString("meetingPassword");
                                sdkKey = "" + jsonObject1.getString("sdkKey");
                                secretKey = "" + jsonObject1.getString("sdkSecret");
                                youtube_url = "" + jsonObject.getString("youtube_url");

                            } else {
                                Toast.makeText(ActivityLiveNew.this, getResources().getString(R.string.NoClassAvailable), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        ProjectUtils.pauseProgressDialog();

                    }
                });
    }



 /*   private void sendMessage(String msg) {
        mDatabase.child("123").child("chat").child("byStudent").setValue(msg);
        MessageModel.DataBean message=new MessageModel.DataBean();
        message.setMessage(msg);
        message.setSide("R");
        message.setMessage_type("1");
        chatAdapter.addMessage(message);

    }*/

    private void unJoinStudent(String studentId) {
        mDatabase.child(modelLogin.getStudentData().getBatchId()).child("joinedStudents").child(studentId).removeValue();
    }


    @Override
    protected void onPause() {
        super.onPause();
        unJoinStudent(modelLogin.getStudentData().getStudentId());
        leaveChannel();
    }

    @Override
    protected void onResume() {

        if (mDatabase != null) {
            joinStudent(modelLogin.getStudentData().getStudentId());
        }
        super.onResume();
    }

    void getQuestion() {

        dialogQuestions = new DialogQuestions(ActivityLiveNew.this);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConsts.STUDENT_ID, modelLogin.getStudentData().getStudentId());
        hashMap.put(AppConsts.BATCH_ID, modelLogin.getStudentData().getBatchId());
        Log.d("asdasd",modelLogin.getStudentData().getStudentId());
        Log.d("asdasd", modelLogin.getStudentData().getBatchId());
        Log.d("asdasd",AppConsts.BASE_URL + AppConsts.API_GET_QUESTION);
        ProjectUtils.showProgressDialog(ActivityLiveNew.this, true, "" + getResources().getString(R.string.Loading___));
        AndroidNetworking.post(AppConsts.BASE_URL + AppConsts.API_GET_QUESTION).addStringBody(new Gson().toJson(hashMap))
                .build()
                .getAsObject(ModelQuestion.class, new ParsedRequestListener<ModelQuestion>() {
                    @Override
                    public void onResponse(ModelQuestion response) {
                        ProjectUtils.pauseProgressDialog();
                        if (response.isStatus()) {
                            if (response.isStatus()) {
                                if (response.getData() != null) {
                                    if (response.getData().size() != 0) {
                                        if (dialogQuestions != null) {
                                            dialogQuestions.dismissDialog();
                                            Log.d("asdsadasd",new Gson().toJson(response.getData().get(0)));
                                            dialogQuestions.showDialog(response.getData().get(0), new DialogQuestions.Callbackk() {
                                                @Override
                                                public void onAutoShowResults(int duration) {
                                                    startCountDwn(duration * 1000, response.getData().get(0).getId());

                                                }
                                            }, numberMeeting);

                                        } /*else {


                                            dialogQuestions.showDialog(response.getData().get(0), new DialogQuestions.Callbackk() {


                                                @Override
                                                public void onAutoShowResults(int duration) {
                                                    startCountDwn(duration * 1000, response.getData().get(0).getId());

                                                }
                                            }, numberMeeting);
                                        }
*/

                                    }
                                }
                            }
                        } else {
                            ProjectUtils.pauseProgressDialog();
                            makeToast(response.getMessage());

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        ProjectUtils.pauseProgressDialog();
                        makeToast(anError.getMessage());
                    }
                });
    }

/*
    private void activateChatListener() {
        mDatabase.child("123").child("chat").child("byTeacher").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
               if (!isFirstTime){
                   MessageModel.DataBean message=new MessageModel.DataBean();
                   message.setMessage(snapshot.getValue().toString());
                   message.setSide("L");
                   message.setMessage_type("1");
                   chatAdapter.addMessage(message);
               }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
*/


    private void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void startCountDwn(long milliseconds, String questionId) {
        new CountDownTimer(milliseconds, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                apiHitResult(questionId);

            }

        }.start();
    }

    public void apiHitResult(String questionId) {
        ProjectUtils.showProgressDialog(this, true, getResources().getString(R.string.Loading___));
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConsts.STUDENT_ID, modelLogin.getStudentData().getStudentId());
        hashMap.put("question_id", questionId);
        Log.d("asd", questionId);
        AndroidNetworking.post(AppConsts.BASE_URL + AppConsts.API_GET_RESULT_OF_QUESTION)
                .addStringBody(new Gson().toJson(hashMap))
                .build()
                .getAsObject(ModelResult.class, new ParsedRequestListener<ModelResult>() {

                    @Override
                    public void onResponse(ModelResult response) {
                        ProjectUtils.pauseProgressDialog();
                        if (response != null) {
                            if (response.isStatus()) {
                                if (response.getData() != null) {
                                    if (response.getData().size() != 0) {
                                        Log.d("adsasd",new Gson().toJson(response.getData().get(0)));
                                        ModelResult.DataBean modelResult = response.getData().get(0);
                                        BottomSheetDialogResult bottomSheetDialogResult = new BottomSheetDialogResult(ActivityLiveNew.this, modelResult);
                                        bottomSheetDialogResult.showDialog();
                                    }
                                }
                            }
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        ProjectUtils.pauseProgressDialog();
                    }
                });
    }

    public void getChat() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConsts.STUDENT_ID, modelLogin.getStudentData().getStudentId());
        hashMap.put(AppConsts.BATCH_ID, modelLogin.getStudentData().getBatchId());
        AndroidNetworking.post(AppConsts.BASE_URL + AppConsts.API_GET_MESSAGE)
                .addStringBody(new Gson().toJson(hashMap))
                .build()
                .getAsObject(MessageModel.class, new ParsedRequestListener<MessageModel>() {
                    @Override
                    public void onResponse(MessageModel response) {
                        if (response != null) {
                            if (response.isStatus()) {
                                ArrayList<MessageModel.DataBean> dataBeans = new ArrayList<>(response.getData());
                                chatAdapter.addAllMessage(dataBeans);
                                binding.recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
                                if (isFirstTimeChatApiLods) {
                                    if (chatAdapter.list.size()>0){
                                        binding.recyclerView.smoothScrollToPosition(chatAdapter.list.size() - 1);
                                    }
                                    isFirstTimeChatApiLods = false;
                                }

                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                    }
                });
    }

    public void sendMessage(String msg) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConsts.STUDENT_ID, modelLogin.getStudentData().getStudentId());
        hashMap.put(AppConsts.BATCH_ID, modelLogin.getStudentData().getBatchId());
        hashMap.put("text", msg);
        AndroidNetworking.post(AppConsts.BASE_URL + AppConsts.API_SEND_MESSAGE)
                .addStringBody(new Gson().toJson(hashMap))
                .build()
                .getAsObject(ModelSuccess.class, new ParsedRequestListener<ModelSuccess>() {

                    @Override
                    public void onResponse(ModelSuccess response) {
                        if (response != null) {
                            if (response.isStatus()) {
                                MessageModel.DataBean message = new MessageModel.DataBean();
                                message.setText(msg);
                                message.setIs_admin("0");
                                message.setMsg_type("1");
                                message.setCreated_at(getCurrentTime());
                                chatAdapter.addMessage(message);
                                // Save state
                                binding.recyclerView.smoothScrollToPosition(chatAdapter.list.size() - 1);


                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                    }
                });
    }


    public String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);

    }

    private void handRaise(Boolean isHandRaisee, String studentId) {
        if (isHandRaisee) {
            binding.ivHandRaise.setVisibility(View.VISIBLE);
            binding.ivHandDown.setVisibility(View.GONE);
            Log.d("asfgg","asdfasd");
            mDatabase.child(modelLogin.getStudentData().getBatchId()).child("joinedStudents").child(studentId).child(("isHandRaised")).setValue("1");
        } else {
            binding.ivHandRaise.setVisibility(View.GONE);
            binding.ivHandDown.setVisibility(View.VISIBLE);
            mDatabase.child(modelLogin.getStudentData().getBatchId()).child("joinedStudents").child(studentId).child(("isHandRaised")).setValue("0");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCommenthide:

                hideComments(true);


                break;
            case R.id.ivCommentShow:
                hideComments(false);
                break;
        }
    }

    @Override
    public void onClickOnVoiceMessage(String audiopath) {
        AudioplayerDialog audioplayerDialog = new AudioplayerDialog(this, new AudioplayerDialog.Callbackk() {
            @Override
            public void isAudioPlaying(Boolean isPlaying) {
                if (!isPlaying) {
                    youTubePlayerr.unMute();
                }
            }
        });

        audioplayerDialog.showDialog(audiopath);
        youTubePlayerr.mute();
    }


    private void initAgoraEngineAndJoinChannel() {
        initializeAgoraEngine();
    }


    private RtcEngine mRtcEngine;
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {

        // Listen for the onUserOffline callback.
        // This callback occurs when the remote user leaves the channel or drops offline.
        @Override
        public void onUserOffline(final int uid, final int reason) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                //    onRemoteUserLeft(uid, reason);
                }
            });
        }

        // Listen for the onUserMuterAudio callback.
        // This callback occurs when a remote user stops sending the audio stream.
        @Override
        public void onUserMuteAudio(final int uid, final boolean muted) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                 //   onRemoteUserVoiceMuted(uid, muted);
                }
            });
        }

    };



    // Call the create method to initialize RtcEngine.
    private void initializeAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
        } catch (Exception e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    // Java
    private void joinChannel() {
        String accessToken = "006a27d77e1ec7b448cbb95b7ae9a0bc3bcIACBgpqkx32TZcaGlKL/9DchHs+z3vB1+WSTSi99DXPf0OzsqawAAAAAEACLYbXRMjC6YAEAAQAxMLpg";
        if (TextUtils.equals(accessToken, "") || TextUtils.equals(accessToken, "#YOUR ACCESS TOKEN#")) {
            accessToken = null; // default, no token
        }

        // Call the joinChannel method to join a channel.
        // The uid is not specified. The SDK will assign one automatically.
        mRtcEngine.joinChannel(accessToken, "jeelink", "Extra Optional Data", 0);
    }

    // Java
    private void leaveChannel() {
        mRtcEngine.leaveChannel();
    }



    public void onAudioMuteClickedSelf(View view) {
        ImageView btn = (ImageView) view;
        if (btn.isSelected()) {
            btn.setSelected(false);
            btn.setImageResource(R.drawable.audio_toggle_btn);
        } else {
            btn.setSelected(true);
            btn.setImageResource(R.drawable.audio_toggle_active_btn);
        }


        mRtcEngine.muteLocalAudioStream(btn.isSelected());
    }

    public void onVideoMuteClickedSelf(View view) {
        ImageView btn = (ImageView) view;
        if (btn.isSelected()) {
            btn.setSelected(false);
            btn.setImageResource(R.drawable.video_toggle_btn);
        } else {
            btn.setSelected(true);
            btn.setImageResource(R.drawable.video_toggle_active_btn);
        }
    /*    mRtcEngine.muteLocalVideoStream(btn.isSelected());
        FrameLayout container = findViewById(R.id.floating_video_container);
        container.setVisibility(btn.isSelected() ? View.GONE : View.VISIBLE);
        SurfaceView videoSurface = (SurfaceView) container.getChildAt(0);
        videoSurface.setZOrderMediaOverlay(!btn.isSelected());
        videoSurface.setVisibility(btn.isSelected() ? View.GONE : View.VISIBLE);*/
    }


}