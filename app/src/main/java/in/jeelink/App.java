package in.jeelink;

import android.app.Application;
import android.content.Context;



import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;

public class App extends Application {
    private Context appContext;
    private Gson gson;
    private static App applicationInstance;
    public static App getInstance() {
        return applicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationInstance = this;
        appContext = this;
        gson = new Gson();
        FirebaseApp.initializeApp(appContext);



    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //  MultiDex.install(this);
    }

    public Gson getGson() {
        return gson;
    }


}
